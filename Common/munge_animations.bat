@REM munge_animations.bat
@REM Calls Animations\%1\munge.bat for all animation subdirectories
@call munge_animation.bat gam_inf_gamorreanguard
@call munge_animation.bat gam_inf_gamorreanguard_lowrez
@call munge_animation.bat tat3_prop_celldoor
@call munge_animation.bat tat3_prop_trapdoor
@call munge_animation.bat tat3_bldg_rancor
@call munge_animation.bat tat3_bldg_torturedroid
@call munge_animation.bat tat3_bldg_jabba